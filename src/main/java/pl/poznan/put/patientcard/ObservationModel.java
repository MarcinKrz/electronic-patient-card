package pl.poznan.put.patientcard;

import javafx.beans.property.SimpleStringProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ObservationModel {
    private SimpleStringProperty id = new SimpleStringProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private BigDecimal value;
    private SimpleStringProperty code = new SimpleStringProperty();
    private Date issued;
    private SimpleStringProperty issuedString = new SimpleStringProperty();

    public String getIssuedString() {
        return issuedString.get();
    }

    public SimpleStringProperty issuedStringProperty() {
        return issuedString;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public void setCode(String code) {
        this.code.set(code);
    }

    public void setIssued(Date issued) {
        this.issued = issued;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        this.issuedString.set(format.format(issued));
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public String getCode() {
        return code.get();
    }

    public SimpleStringProperty codeProperty() {
        return code;
    }

    public Date getIssued() {
        return issued;
    }
}
