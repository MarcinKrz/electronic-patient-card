package pl.poznan.put.patientcard.controllers;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.gclient.StringClientParam;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.Setter;
import lombok.var;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import pl.poznan.put.patientcard.MedicationRequestModel;
import pl.poznan.put.patientcard.ObservationModel;
import pl.poznan.put.patientcard.PatientModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Client {

    @Setter
    private IGenericClient client;

    public Client() {
        FhirContext ctx = FhirContext.forDstu3();
//        String serverBase = "http://hapi.fhir.org/baseDstu3";
        String serverBase = "http://localhost:8080/baseDstu3";
        client = ctx.newRestfulGenericClient(serverBase);
    }

    public List<Bundle.BundleEntryComponent> getEverything(String patientID) {
        Parameters outParams = client
                .operation()
                .onInstance(new IdType("Patient", patientID))
                .named("$everything")
                .withNoParameters(Parameters.class)
                .useHttpGet()
                .execute();

        List<Bundle.BundleEntryComponent> entries = new LinkedList<>();
        List<Parameters.ParametersParameterComponent> parameterComponents = outParams.getParameter();

        for (Parameters.ParametersParameterComponent parameterComponent : parameterComponents) {
            Bundle bundle = (Bundle) parameterComponent.getResource();
            entries.addAll(bundle.getEntry());
            while (bundle.getLink(Bundle.LINK_NEXT) != null) {
                // load next page
                bundle = client.loadPage().next(bundle).execute();
                entries.addAll(bundle.getEntry());
            }
        }
        return entries;
    }




    public ObservableList<PatientModel> getPatients() {
        Bundle result = client.search().
                forResource(Patient.class)
                .where(new StringClientParam("name").matches().value("a"))
                .count(50)
                .returnBundle(Bundle.class).execute();
        ArrayList<Patient> patients = new ArrayList<>();

        for (Bundle.BundleEntryComponent patient : result.getEntry()) {
            patients.add((Patient) patient.getResource());
        }

        ObservableList<PatientModel> patientModels = FXCollections.observableArrayList(new ArrayList<PatientModel>());
        for (Patient patient : patients) {
            var id = patient.getId().split("/")[5];
            var firstName = patient.getNameFirstRep().getGiven().get(0).toString();
            var lastName = patient.getNameFirstRep().getFamily();
            var birthDate = patient.getBirthDate();
            var gender = patient.getGender().getDisplay();
            var city = patient.getAddress().get(0).getCity();
//            var adress = Optional.ofNullable(patient.getAddress()).igetAddress().get(0).city;
            patientModels.add(new PatientModel(id, firstName, lastName, birthDate, gender, city));

        }
        return patientModels;
    }


    public List<Medication> getMedication(List<Bundle.BundleEntryComponent> entries) {
        List<Medication> medicationList = new LinkedList<>();
        for (Bundle.BundleEntryComponent e : entries) {
            if (e.getResource() instanceof Medication) {
                Medication m = (Medication) e.getResource();
//                Medication myMedication = new myMedication(m);
                medicationList.add(m);
            }
        }
        return medicationList;
    }


    public List<Observation> getObservation(List<Bundle.BundleEntryComponent> entries) {
        List<Observation> observationList = new LinkedList<>();

        for (Bundle.BundleEntryComponent e : entries) {
            if (e.getResource() instanceof Observation) {
                Observation o = (Observation) e.getResource();
//                myObservation myObservation = new myObservation(o);
                observationList.add(o);
            }
        }
        return observationList;
    }

    public List<ObservationModel> getObservationModels(List<Observation> observations) {
        List<ObservationModel> observationModels = new ArrayList<>();
        observations.forEach(e -> {
            var ob = new ObservationModel();
            ob.setId(e.getId().split("/")[5]);
            ob.setName(e.getCode().getText());

            if (e.hasComponent()) {
//                e.getComponent().forEach(component ->
                {
                    try {
                        ob.setName(e.getComponent().get(0).getCode().getText());
                        ob.setValue(e.getComponent().get(0).getValueQuantity().getValue());
                        ob.setCode(e.getComponent().get(0).getValueQuantity().getCode());
                    } catch (FHIRException e1) {
                        e1.printStackTrace();
                    }
                }
//                );
            }
            if (e.hasValueQuantity()) {
                try {
                    ob.setValue(e.getValueQuantity().getValue());
                    ob.setCode(e.getValueQuantity().getCode());
                } catch (FHIRException e1) {
                    e1.printStackTrace();
                }
            }
            if (e.hasIssued()) {
                ob.setIssued(e.getIssued());
            }
            if (ob.getId() == null || ob.getName() == null || ob.getValue() == null || ob.getCode() == null || ob.getIssued() == null) {

            } else {
                observationModels.add(ob);
            }

        });

        return observationModels;
    }

    public List<MedicationRequest> getMedicationRequest(List<Bundle.BundleEntryComponent> entries) {
        List<MedicationRequest> medicationRequestList = new LinkedList<>();

        for (Bundle.BundleEntryComponent e : entries) {
            if (e.getResource() instanceof MedicationRequest) {
                MedicationRequest mr = ((MedicationRequest) e.getResource());
                medicationRequestList.add(mr);
            }
        }
        return medicationRequestList;
    }

    public List<MedicationRequestModel> getMedicationRequestModels(List<MedicationRequest> observations) {
        List<MedicationRequestModel> medicationRequestModels = new ArrayList<>();
        observations.forEach(e -> {
            var medReq = new MedicationRequestModel();
            medReq.setId(e.getId().split("/")[5]);
            medReq.setName(((CodeableConcept) e.getMedication()).getText());
            medReq.setStatus(e.getStatus().getDisplay());
            medReq.setIntent(e.getIntent().getDisplay());
            medReq.setDate(e.getAuthoredOn());

            if (medReq.getId() == null || medReq.getName() == null || medReq.getStatus() == null || medReq.getIntent() == null || medReq.getDate() == null) {

            } else {
                medicationRequestModels.add(medReq);
            }

        });
    return medicationRequestModels;
    }
}
