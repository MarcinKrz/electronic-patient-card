package pl.poznan.put.patientcard.controllers;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import lombok.Setter;
import lombok.var;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.MedicationRequest;
import org.hl7.fhir.dstu3.model.Observation;
import pl.poznan.put.patientcard.DateAxis;
import pl.poznan.put.patientcard.MedicationRequestModel;
import pl.poznan.put.patientcard.ObservationModel;
import pl.poznan.put.patientcard.PatientModel;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.util.*;

@Setter
public class PatientController {
    private Client client;
    private PatientModel patientModel;

    @FXML
    private Label firstNameText;

    @FXML
    private Label lastNameText;

    @FXML
    private Label genderText;

    @FXML
    private Label birthDateText;


    @FXML
    private Label cityText;

    @FXML
    StackPane stackPane;

    @FXML
    JFXDatePicker startDatePicker;

    @FXML
    JFXDatePicker endDatePicker;


    @FXML
    private TableView<ObservationModel> observationsTable;

    @FXML
    private TableColumn<ObservationModel, String> dateIssuedColumn;

    @FXML
    private TableColumn<ObservationModel, String> nameColumn;

    @FXML
    private VBox vBoxToChart;

    @FXML
    private ChoiceBox<String> choiceBox;

    LineChart<Date, Number> chart;

    private List<Bundle.BundleEntryComponent> patientsEverything;

    private List<Observation> observations;

    private List<MedicationRequest> medicationRequests;

    private FilteredList<MedicationRequestModel> filteredListMedReqModel;

    private FilteredList<ObservationModel> filteredObserList;

    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat format2 = new SimpleDateFormat("yyyy/MM/dd hh:mm");
    @FXML
    private Tab tabPane1;
    @FXML
    private Tab tabPane2;
    @FXML
    private Tab tabPane3;
    @FXML
    private TableView<MedicationRequestModel> medicationRequestTable;

    @FXML
    private TableColumn<MedicationRequestModel, String> dateColumn;

    @FXML
    private TableColumn<MedicationRequestModel, String> nameColumnMedReq;

    @FXML
    public void initialize() {
        firstNameText.setText(patientModel.getFirstName());
        lastNameText.setText(patientModel.getLastName());


        birthDateText.setText(format.format(patientModel.getBirthDate()));
        cityText.setText(patientModel.getCity());
        genderText.setText(patientModel.getGender());
        patientsEverything = client.getEverything(patientModel.getId());
        var a = client.getMedication(patientsEverything);
        medicationRequests = client.getMedicationRequest(patientsEverything);
        observations = client.getObservation(patientsEverything);


        setUpDatePickers();


        showObservations();
        showMedicationRequest();
        ObservableList<String> strings = FXCollections.observableArrayList(prepareDataToChart().keySet());
        choiceBox.setItems(strings);
        choiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
            if (newValue != null) {
                createChart(newValue, prepareDataToChart().get(newValue));
            } else {
                vBoxToChart.getChildren().clear();
            }
        });

        choiceBox.setVisible(false);
        tabPane1.setOnSelectionChanged(event -> {
            stackPane.getChildren().clear();
            choiceBox.setVisible(false);
        });
        tabPane2.setOnSelectionChanged(event -> {
            stackPane.getChildren().clear();
            choiceBox.setVisible(false);
        });
        tabPane3.setOnSelectionChanged(event -> {
            stackPane.getChildren().clear();
            choiceBox.setVisible(true);
        });

    }

    private class ElementChart {
        public String name;
        public Date date;
        public Number value;

    }

    private void createChart(String title, List<ElementChart> data) {
        ObservableList<XYChart.Series<Date, Number>> series = FXCollections.observableArrayList();

        ObservableList<XYChart.Data<Date, Number>> series1Data = FXCollections.observableArrayList();

        data.forEach(e -> {
            series1Data.add(new XYChart.Data<Date, Number>(e.date, e.value));
        });
//        series1Data.add(new XYChart.Data<Date, Number>(new GregorianCalendar(2014, 5, 3).getTime(), 4));

        series.add(new XYChart.Series<>(title, series1Data));


        NumberAxis numberAxis = new NumberAxis();
        DateAxis dateAxis = new DateAxis();
        chart = new LineChart<>(dateAxis, numberAxis, series);

        for (var entry : series1Data) {
            Tooltip t = new Tooltip(format2.format(entry.getXValue())+"\n"+entry.getYValue().toString());
            Tooltip.install(entry.getNode(), t);
            entry.getNode().setOnMouseEntered(event -> {
                entry.getNode().getStyleClass().add("onHover");
            });

            //Removing class on exit
            entry.getNode().setOnMouseExited(event -> {
                entry.getNode().getStyleClass().remove("onHover");
            });
        }

        vBoxToChart.getChildren().clear();
        vBoxToChart.getChildren().add((Node) chart);


    }

    private void setUpDatePickers() {
        startDatePicker.setOnAction((event -> {
            Instant instant = Instant.from((startDatePicker.getValue().atStartOfDay(ZoneId.systemDefault())));
            Date date = Date.from(instant);
            filteredListMedReqModel.setPredicate(medReq -> {
                {
                    return medReq.getDate().after(date);
                }
            });
            filteredObserList.setPredicate(obser -> {
                return obser.getIssued().after(date);
            });
            ObservableList<String> strings = FXCollections.observableArrayList(prepareDataToChart().keySet());
            choiceBox.setItems(strings);

        }));

        endDatePicker.setOnAction((event -> {
            Instant instant = Instant.from((endDatePicker.getValue().atStartOfDay(ZoneId.systemDefault())));
            Date date = Date.from(instant);
            filteredListMedReqModel.setPredicate(medReq -> {
                {
                    return medReq.getDate().before(date);
                }
            });
            filteredObserList.setPredicate(obser -> {
                return obser.getIssued().before(date);
            });
            ObservableList<String> strings = FXCollections.observableArrayList(prepareDataToChart().keySet());
            choiceBox.setItems(strings);

        }));
    }

    public Map<String, List<ElementChart>> prepareDataToChart() {
        Map<String, List<ElementChart>> objects = new HashMap<>();
        filteredObserList.forEach(ob -> {
            var elem = new ElementChart();
            elem.name = ob.getName() + " [ " + ob.getCode() + " ]";
            elem.value = ob.getValue();
            elem.date = ob.getIssued();
            if (objects.containsKey(elem.name)) {
                objects.get(elem.name).add(elem);
            } else {
                List<ElementChart> newList = new ArrayList<>();
                newList.add(elem);
                objects.put(elem.name, newList);
            }
        });


        return objects;
    }

    public void showMedicationRequest() {
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("dateString"));
        nameColumnMedReq.setCellValueFactory(new PropertyValueFactory<>("name"));
        List<MedicationRequestModel> medReqModels = client.getMedicationRequestModels(medicationRequests);

        ObservableList<MedicationRequestModel> medReqToTable = FXCollections.observableArrayList(medReqModels);
        filteredListMedReqModel = new FilteredList<>(medReqToTable);
        SortedList<MedicationRequestModel> sortableData = new SortedList<>(filteredListMedReqModel);
        sortableData.comparatorProperty().bind(medicationRequestTable.comparatorProperty());
        dateColumn.setSortType(TableColumn.SortType.ASCENDING);
        medicationRequestTable.setItems(sortableData);
        medicationRequestTable.getSortOrder().add(dateColumn);

        medicationRequestTable.setRowFactory(tv -> {
            TableRow<MedicationRequestModel> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    MedicationRequestModel selectedItem = row.getItem();
                    var text = "Name: " + selectedItem.getName() + "\n";
                    text += "Date: " + selectedItem.getDateString() + "\n";
                    text += "Status: " + selectedItem.getStatus() + "\n";
                    text += "Intent: " + selectedItem.getIntent() + "\n";
                    showDialog("Medication Request Details", text);

                }
            });

            return row;
        });

    }

    public void showObservations() {
        dateIssuedColumn.setCellValueFactory(new PropertyValueFactory<>("issuedString"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        List<ObservationModel> observationModel = client.getObservationModels(observations);
        ObservableList<ObservationModel> observationsToTable = FXCollections.observableArrayList(observationModel);
        filteredObserList = new FilteredList<>(observationsToTable);
        SortedList<ObservationModel> sortableData = new SortedList<>(filteredObserList);
        sortableData.comparatorProperty().bind(observationsTable.comparatorProperty());
        dateIssuedColumn.setSortType(TableColumn.SortType.ASCENDING);
        observationsTable.setItems(sortableData);
        observationsTable.getSortOrder().add(dateIssuedColumn);

        observationsTable.setRowFactory(tv -> {
            TableRow<ObservationModel> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    ObservationModel selectedItem = row.getItem();
                    var text = "Name: " + selectedItem.getName() + "\n";
                    text += "Value: " + selectedItem.getValue() + "\n";
                    text += "Unit: " + selectedItem.getCode() + "\n";
                    text += "Date: " + selectedItem.getIssuedString() + "\n";
                    showDialog("Observation Details", text);

                }
            });

            return row;
        });

    }

    public void showDialog(String title, String text) {
        stackPane.getChildren().clear();
        JFXDialogLayout content = new JFXDialogLayout();
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);

        content.setHeading(new Text(title));
        content.setBody(new Text(text));
//        content.setActions(dialog);
        dialog.show();
    }

}
