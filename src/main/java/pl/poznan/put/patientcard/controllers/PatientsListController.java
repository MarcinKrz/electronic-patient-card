package pl.poznan.put.patientcard.controllers;


import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lombok.Setter;
import lombok.var;
import pl.poznan.put.patientcard.PatientModel;

import java.io.IOException;


public class PatientsListController {

    @Setter
    private Client client;

    @FXML
    private TableView<PatientModel> patientsTable;

    @FXML
    private TableColumn<PatientModel, String> firstNameColumn;

    @FXML
    private TableColumn<PatientModel, String> lastNameColumn;

    @FXML
    private TextField filterLastNameTextField;
    @FXML
    private VBox vBoxListPatients;


    @FXML
    public void initialize() {
        client = new Client();
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        var allPatients = client.getPatients();
        FilteredList<PatientModel> filteredList = new FilteredList<>(allPatients);

        filterLastNameTextField.textProperty().addListener(((observable, oldValue, newValue) -> filteredList.setPredicate(patient -> {
            {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();

                return patient.getLastName().toLowerCase().contains(lowerCaseFilter);
            }
        })));
        patientsTable.setItems(filteredList);

        patientsTable.setRowFactory(tv -> {
            TableRow<PatientModel> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    PatientModel selectedPatient = row.getItem();

                    FXMLLoader root;
                    try {
                        root = new FXMLLoader(getClass().getClassLoader().getResource("patient.fxml"));

                        PatientController controller = new PatientController();
                        controller.setClient(client);
                        controller.setPatientModel(selectedPatient);
                        root.setController(controller);

                        Parent parent = root.load();
                        Stage stage = new Stage();
                        stage.setTitle("Patient's Details");
                        var scene = new Scene(parent, 700,600);
                        scene.getStylesheets().add("style.css");
                        stage.setScene(scene);
                        stage.setResizable(false);

                        stage.show();
                        // Hide this current window (if this is what you want)
//                        ((Node) (event.getSource())).getScene().getWindow().hide();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            return row;
        });

        patientsTable.prefWidthProperty().bind(vBoxListPatients.prefWidthProperty());
        patientsTable.prefHeightProperty().bind(vBoxListPatients.prefHeightProperty());
    }
}