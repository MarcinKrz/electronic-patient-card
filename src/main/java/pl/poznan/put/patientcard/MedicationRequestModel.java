package pl.poznan.put.patientcard;

import javafx.beans.property.SimpleStringProperty;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MedicationRequestModel {

    private SimpleStringProperty id = new SimpleStringProperty();
    private SimpleStringProperty name  = new SimpleStringProperty();
    private SimpleStringProperty status = new SimpleStringProperty();
    private SimpleStringProperty intent = new SimpleStringProperty();
    private Date date = new Date();
    private SimpleStringProperty dateString = new SimpleStringProperty();

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public String getStatus() {
        return status.get();
    }

    public SimpleStringProperty statusProperty() {
        return status;
    }

    public String getIntent() {
        return intent.get();
    }

    public SimpleStringProperty intentProperty() {
        return intent;
    }

    public Date getDate() {
        return date;
    }

    public String getDateString() {
        return dateString.get();
    }

    public SimpleStringProperty dateStringProperty() {
        return dateString;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public void setIntent(String intent) {
        this.intent.set(intent);
    }

    public void setDate(Date date) {
        this.date = date;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        this.dateString.set(format.format(date));
    }

}
