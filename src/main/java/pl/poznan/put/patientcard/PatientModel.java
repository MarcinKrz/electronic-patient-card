package pl.poznan.put.patientcard;

import javafx.beans.property.SimpleStringProperty;
import lombok.Getter;

import java.util.Date;


public class PatientModel {
    private SimpleStringProperty id;
    private SimpleStringProperty firstName;
    private SimpleStringProperty lastName;
    private Date birthDate;
    private SimpleStringProperty gender;
    private SimpleStringProperty city;


    public PatientModel(String id, String firstName, String lastName, Date birthDate, String gender, String city) {
        this.id = new SimpleStringProperty(id);
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.birthDate = birthDate;
        this.gender = new SimpleStringProperty(gender);
        this.city = new SimpleStringProperty(city);
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setGender(String gender) {
        this.gender.set(gender);
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public SimpleStringProperty firstNameProperty() {
        return firstName;
    }

    public String getLastName() {
        return lastName.get();
    }

    public SimpleStringProperty lastNameProperty() {
        return lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getGender() {
        return gender.get();
    }

    public SimpleStringProperty genderProperty() {
        return gender;
    }

    public String getCity() {
        return city.get();
    }

    public SimpleStringProperty cityProperty() {
        return city;
    }
}
